/**
 * Tom Bielawski
 * 11/5/2020
 * Gaddis Early Objects Program 11-1
 * Use of Structures in a payroll program
 * */


#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

/*Struct Definition*/
struct PayRoll
{
    int employeeNumber;
    string employeeName;
    double hoursWorked;
    double employeePayRate;
    double employeeGrossPay;
};

/*Main*/
int main()
{
    //Creating a structure called employee from PayRoll
    PayRoll employee;

    //Enter the employee number
    cout << "Please enter the employee's number. " << endl;
    //Use the struct to collect the data
    cin >> employee.employeeNumber;


    //Enter the employee name
    cout << "Please enter the employee's name. " << endl;
    //Used with getline function, ignores next character in input buffer
    //Without cin.ignore(), employee name is skipped.
    cin.ignore();

    //Use getline to take the string employees full name
    //Clion IDE automatically places & before cin and employee.name
    getline(cin, employee.employeeName);

    //Get the hours worked using the structure
    cout << "Please enter the hours that this employee worked. " << endl;
    cin >> employee.hoursWorked;

    //Get the hourly pay rate for the employee
    cout << "Please enter this employee's hourly pay rate. " << endl;
    cin >> employee.employeePayRate;

    //Do the math
    employee.employeeGrossPay = employee.hoursWorked * employee.employeePayRate;

    //Output the employee data
    cout << "Employee: " << employee.employeeName
         <<"\nEmployee Number: " << employee.employeeNumber
         <<"\nHours Worked: " << employee.hoursWorked
         <<"\nHourly Pay Rate: " << employee.employeePayRate << endl;

    cout << fixed << showpoint << setprecision (2);
    cout << "\nGross Pay: $" << employee.employeeGrossPay << endl;


    return 0;
}
