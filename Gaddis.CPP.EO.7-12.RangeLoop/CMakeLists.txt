cmake_minimum_required(VERSION 3.17)
project(Gaddis_CPP_EO_7_12_RangeLoop)

set(CMAKE_CXX_STANDARD 14)

add_executable(Gaddis_CPP_EO_7_12_RangeLoop main.cpp)