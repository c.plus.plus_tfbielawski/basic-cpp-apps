/*
 * Tom Bielawski
 * 10/27/2020
 * Gaddis CPP Early Objects
 * Section 7-12 Program 7-25, 7-26
 * Range based for-loop
 * */

#include <iostream>
#include <vector>
using namespace std;

/*Main Method*/
int main()
{
    //Define a vector of integers with 5 elements
    vector<int> numbersVector(5);

    //Get the values for the vector elements using for each loop
    for (int &val : numbersVector)
    {
        cout << "Enter an integer value: " << endl;
        cin >> val;
    }

    //Display the vector elements using for-each loop
    cout << "You entered: " << endl;
    for (int val : numbersVector)
        cout << val << endl;
    return 0;
}
