/*
 * Tom Bielawski
 * 10/27/2020
 * Gaddis CPP Early Objects
 * Section 7-12 Program 7-24
 * Storing and retrieving values in a vector
 * */

#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

/*Main Method*/
int main()
{
    const int NUM_EMPLOYEES {5};            //Const for num of employees
    vector<int> hours(NUM_EMPLOYEES);       //int vector
    vector<double> payRate(NUM_EMPLOYEES);  //double vector

    //Ask user for input
    cout << "Enter the hours worked by " << NUM_EMPLOYEES << endl;
    cout << "\n employees and their hourly rates.\n" << endl;

    //For loop to process vector and output hours one at a time
    for (int index = 0; index < NUM_EMPLOYEES; index++)
    {
        cout << "Hours worked by employee #" << (index + 1) << ":" << endl;
        cin >> hours[index];
        cout << "Hourly pay rate for employee #" << (index + 1) << ":" << endl;
        cin >> payRate[index];
    }

    //Display the employees' gross pay
    cout << "Gross pay for each employee: " << endl;

    //Establish the format for output
    cout << fixed << showpoint << setprecision(2) << endl;

    //For loop to process vector and output gross pay one at a time
    for (int index = 0; index < NUM_EMPLOYEES; index++)
    {
        //Gross pay = employee# hours * employee# pay rate
        double grossPay = hours[index] * payRate[index];
        cout << "Employee #" << (index + 1) << ": $" << grossPay << endl;
    }
    return 0;
}
