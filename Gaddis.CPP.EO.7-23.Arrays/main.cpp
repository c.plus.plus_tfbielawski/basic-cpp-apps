/*
 * Tom Bielawski
 * 10/27/2020
 * Gaddis CPP Early Objects
 * Section 7-11 Program 7-23
 * Compare contents of two arrays
 * */

#include <iostream>
#include <array>
using namespace std;

/*Function Prototype Declaration*/
bool testPin(const int [], const int [] , int);

/*Function Implementation bool testPin()
 * This function accepts 2 int arrays (custPIN and databasePIN)
 * and compares them. If they contain the same values, return true.
 * Otherwise, return false.
 * */
bool testPin(const int custPIN[], const int databasePIN[], int size)
{
    for (int index = 0; index < size; index++)
    {
        //If values are not the same, return false
        if (custPIN[index] != databasePIN[index])
            return false;
    }
    //Otherwise return true
    return true;

}

/*Main Method*/
int main()
{
    //Declare constant for number of digits in a PIN
    const int NUM_DIGITS {7};
    //Int array declarations, initialized to base set of values
    //for comparison
    int pin1[NUM_DIGITS] = {2, 4, 1, 8, 7, 9, 0};
    int pin2[NUM_DIGITS] = {2, 4, 6, 8, 7, 9, 0}; //One element difference
    int pin3[NUM_DIGITS] = {1, 2, 3, 4, 5, 6, 7}; //All elements different

    //Compare pin 1 and 2
    if (testPin(pin1, pin2, NUM_DIGITS))
        cout << "Error: PIN 1 and PIN 2 are the same." << endl;
    else
        cout << "SUCCESS: PIN 1 and PIN 2 are different." << endl;

    //Compare Pin 1 and 3
    if (testPin(pin1, pin3, NUM_DIGITS))
        cout << "Error: PIN 1 and PIN 3 are the same." << endl;
    else
        cout << "SUCCESS: PIN 1 and PIN 3 are different." << endl;

    //Compare pin 1 and pin 1
    if (testPin(pin1, pin1, NUM_DIGITS))
        cout << "SUCCESS: PIN 1 and PIN 1 are the same." << endl;
    else
        cout << "ERROR: PIN 1 and PIN 1 are different." << endl;

    return 0;
}
