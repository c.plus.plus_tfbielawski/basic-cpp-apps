/**
 * Tom Bielawski
 * 10/27/2020
 * Gaddis CPP Early Objects
 * Section 7-12 Program 7-28 Size of vector
 * 7-29 Removing elements from vector
 * */

#include <iostream>
#include <vector>
using namespace std;

/*Function Prototype Declaration*/
void showValues(vector<int>);

/**
 * Function Prototype Implementation*
 * Accepts a vector as its argument.
 * Value of each element is displayed
 * */
void showValues(vector<int> vect)
{
    for(int count = 0; count < vect.size(); count++)
    {
        cout << vect[count] << endl;
    }
}

/*Main Program*/
int main()
{
    //Declare a vector named vectorValues
    vector<int> vectorValues;

    //Input a series of numbers into vector
    for (int count = 0; count < 7; count++)
        vectorValues.push_back(count * 2);

    //Display the numbers in the vector
    showValues(vectorValues);
    return 0;
}
