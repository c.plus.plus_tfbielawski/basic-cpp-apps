/**
 * Tom Bielawski
 * 10/29/2020
 * Gaddis CPP Early Objects
 * Section 8-1 Program
 * 8-1 array search list
 * */

#include <iostream>
#include <array>
using namespace std;

/*Function prototype
 * Searching a list. const array of int, int, and int
 * Const int to hold the size of the array*/
int searchList(const int [], int, int);
const int SIZE {5};

/*Function implementation
 * This function performs a linear search in the array.
 * Array list is searched for a specified value. If found,
 * the subscript is returned. If not, -1 is returned.
 * const int list[] to match const int [] in prototype
 * int numberElements to match int in prototype
 * int elementValue to match int in prototype
 * */

int searchList(const int list[], int numberElements, int elementValue )
{
    int index = 0;          //Subscript to search the array
    int position = -1;      //Records the position of the search value
    bool found = false;     //Flag to show if the search value was found

    while (index < numberElements && !found)
    {
        if (list[index] == elementValue) //If the value is found
        {
            found = true;               //Set the flag to true
            position = index;           //Record the value susbcript
        }
                                        //advance the counter to next element
        index++;
    }
    return position;                    //Return the position or return -1
}


int main()
{
    //Declare array called test with SIZE as its size
    //initialize it
    int tests[SIZE] = {87, 75, 98, 100, 82};

    //Declare an int to hold the results of the search
    int results;

    results = searchList(tests, SIZE, 100);

    //If our search returns -1, then the number 100 was not found
    if (results == -1)
        cout << "You did not earn 100% on any test." << endl;
    else
    {
        cout << "You earned 100% on test ";
        //Must add 1 to results because position is initialized with a -1
        cout << "number " << (results + 1) << endl;
    }
    cout << "Hello, World!" << endl;
    return 0;
}
