/**
 * Tom Bielawski
 * 10/29/2020
 * Gaddis CPP Early Objects
 * Section 9-2 Pointers
 * */

// stores the address of a variable in a pointer.
#include <iostream>
using namespace std;

int main()
{
    //Declare and initialize a variable
    int someValue = 77;

    //Declare a pointer-variable and initialize it as null
    int *pointyPointer = nullptr;

    //Store the address of someValue in ptr
    pointyPointer = &someValue;

    cout << "The value in someValue is " << someValue << endl;
    cout << "The address of someValue is " << pointyPointer << endl;
    return 0;
}