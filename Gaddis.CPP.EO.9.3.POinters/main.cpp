/**
 * Tom Bielawski
 * 10/29/2020
 * Gaddis CPP Early Objects
 * Section 9-3 Pointers
 * */

// This program demonstrates the use of the indirection operator.
#include <iostream>
using namespace std;
int main()
{
    int anIntegerVariable = 95;             // integer variable
    int *pointyPointer = nullptr;           // Declare and init a pointer variable
    pointyPointer = &anIntegerVariable;     // Store the address of the integer var in pointyPointer


    // Use integer variable and the pointer to display the value
    cout << "Here is the value in anIntegerVariable, printed twice:\n";
    cout << anIntegerVariable << endl;      // Displays the contents of integer variable
    cout << *pointyPointer << endl;         // Displays the contents of pointer to int variable
    *pointyPointer = 100;                   // Assign 100 to the location pointed to by the pointer.

    cout << "Once again, here is the value in anIntegerVariable:\n";
    cout << anIntegerVariable << endl;

    cout << *pointyPointer << endl;         // Displays the contents of the
    return 0;
}