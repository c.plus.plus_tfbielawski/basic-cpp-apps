//Tom Bielawski
//8/16/2020
//C++ Beginner Projects www.codeboks.com
//Write a C++ program to check if shape is rectangle/square, rhombus, parallelogram, or NA

#include <iostream>
using namespace std;

//Void instead of int main.
int main()
{
    //Declare four variables to hold user-entered data.
    int angle1, angle2, angle3, angle4;

    // escape character \n = new line, \t inserts a tab space
    //Output to begin the program

    //Use cout function to create a "header" for the program
    cout <<"\n\t----------------------------------------\n";
    //\t pushes the output as though a tab key were pressed
    cout <<"\t\t:Program to check a :\n";
    cout <<"\t----------------------------------------\n\n";
    //Ask for input and store in int variables
    cout <<"Enter the angles of your four-sided object. \n";
    cout <<"Enter the angles one at a time, press enter after each one:\n";
    cin >> angle1;
    cin >> angle2;
    cin >> angle3;
    cin >> angle4;
    //Need to add an equation to determine if opposite angles are equal
    //Need to add lengths of sides
    if(angle1 + angle2 + angle3 + angle4 == 360)
    {
        cout << "This is either a square or a rectangle.";
    }
    else
    {
        cout << "This is not a an equilateral object. Please run the program again." << endl;
    }

//Getch() reads a single character from the keyboard but doesn't display
    getchar();
}