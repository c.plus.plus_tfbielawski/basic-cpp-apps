/**
 * Tom Bielawski
 * 11/11/2020
 * Recursive Fibonacci Sequence Function
 * www.javatpoint.com
 * */

#include <iostream>
using namespace std;

/*Recursive Function Defined*/
void fiboNacci(int someInteger)
{
    //Static int so value isn't changed at conclusion of the function
    static int firstNum = 0, secondNum = 1, nextNum;

    //While n greater than 0
    if (someInteger > 0)
    {
        //Assign the sum of 1 and 2
        nextNum = firstNum + secondNum; 

        //Output nextNum, continue the pattern
        cout << nextNum << " ";

        //Assign NUM2 to NUM1
        firstNum = secondNum;           

        //Assign nextNum to secondNum
        secondNum = nextNum;             


        //the function calls itself and subtracts 1 from number entered
        //This is a counting down sequence
        fiboNacci(someInteger - 1);

    }

}

int main()
{
    int fibNumber;
    cout << "Enter number of terms for the Fibonacci Sequence: " << endl;
    cin >> fibNumber;
    cout << "Fibonacci series is: 0 1 ";

    //Call the fiboNacci function with fibNumber -2 as input
    fiboNacci(fibNumber - 2);
    return 0;

}
