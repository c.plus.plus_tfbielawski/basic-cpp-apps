cmake_minimum_required(VERSION 3.17)
project(Misc_C__)

set(CMAKE_CXX_STANDARD 14)

add_executable(Misc_C__ main.cpp)