//Vectors.Cpp
//Tom Bielawski
//09-30-2020
//C++ Beginner to Beyond, Frank Mitropoulos, Udemy, Section 7

#include <iostream>
//Including vectors
#include <vector>
//Using this to avoid scope resolution operator everywhere vector is used
using namespace std;

int main()
{
    vector <char> vowelVector; //empty vector
    vector <char> vectorOfVowels(5); //5 characters in lengh, init to 0
    //vector initialized with characters in each element
    vector <char> vowlesVector { 'a', 'e', 'i', 'o', 'u' };

    //Displaying the characters in elements 1 and 3 using array syntax
    cout << vowlesVector[1] << endl;
    cout << vowlesVector[3] << endl;

    //Vector of integers, intialized
    vector <int> testScores { 100, 98, 89 };

    //Displaying the integers using array syntax
    //Escape character used to create separation from vowels vector output
    cout << "\n" << testScores[0] << endl;
    cout << testScores[1] << endl;
    cout << testScores[2] << endl;

    //Displaying the integers using vector syntax
    //the vectorName.size() method provides the length of the vector
    cout << "\n" << testScores.at(0) << endl;
    cout << testScores.at(1) << endl;
    cout << testScores.at(2) << endl;
    cout << "There are " << testScores.size() << " scores in the testScores vector." << endl;

    cout << "\nPlease enter three test scores to fill the vector." << endl;
    cin >> testScores.at(0);
    cin >> testScores.at(1);
    cin >> testScores.at(2);

    cout << "\nTest scores updated." << endl;
    cout << testScores.at(0) << endl;
    cout << testScores.at(1) << endl;
    cout << testScores.at(2) << endl;

    cout << "\nEnter another test score for the vector: " << endl;
    //Declare variable to hold the new test score, note the (0)
    int newTestScore(0);
    cin >> newTestScore;

    //Pushback method adds a new score the vector and clears the newTestScore variable
    testScores.push_back(newTestScore);

    //Now we can reuse the variable and enter another test score to the vector
    cout << "\nEnter one more test score for our vector: " << endl;
    cin >> newTestScore;
    testScores.push_back(newTestScore);

    cout << "\nThere are now " << testScores.size() << " scores in this vector." << endl;

    //Displaying the new test scores
   /* cout << testScores.at(0) << endl;
    cout << testScores.at(1) << endl;
    cout << testScores.at(2) << endl;
    cout << testScores.at(3) << endl;
    cout << testScores.at(4) << endl; */

    cout << testScores.at(0) << " " << testScores.at(1) << " " << testScores.at(2) << " " << testScores.at(3)
    << " and " << testScores.at(4) << endl;

    //Using an invalid element with vector syntax will cause an exception
    /*cout << "\nThis should cause an exception: testScores.at(25)" << endl;
    cout << testScores.at(25) << endl;*/

    //Using an invalid element with array syntax will crash with no exception: no bounds checking.
    //cout << "\nThis should cause an exception: " << testScores[25] << endl;

    //Example of a 2-dimensional vector, which is a vector of vectors.
    //Creating a vector to hold a string vectors with personnel initials
    vector <vector <string>> employeeInitials
    {
            //Use commas to separate each vector
            { "AA", "BB", "CC", "DD" },
            { "AO", "BO", "CO", "DO" },
            { "BA", "CA", "DA", "EA" }

    //End the vector declaration with a semicolon
    };

    cout << "The first vector of initials { \"AA\", \"BB\", \"CC\", \"DD\" } using array-syntax is: " << endl;
    cout<< employeeInitials [0][0] << " " << employeeInitials [0][1] << " " << employeeInitials [0][2]
            << " and " << employeeInitials [0][3] << endl;

    cout << "The second vector of initials { \"AO\", \"BO\", \"CO\", \"DO\" } using vector-syntax is: " << endl;
    cout<< employeeInitials.at(1).at(0) << " "<< employeeInitials.at(1).at(1) << " "
    << employeeInitials.at(1).at(2) << " and " << employeeInitials.at(1).at(3) << endl;
 }



/*vector_name.at(element_index)/
 * push back method is used to
 * add a new figure into the vector/
 * vector_name.push_back(element) /
 * */

//vector_name.at(element_index)